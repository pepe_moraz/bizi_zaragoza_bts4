Proyecto MVC con PHP en el que, partiendo de un archivo json del repositorio Opendata del Ayto. de Zaragoza, se sirve una lista de todas las estaciones bici y los datos de bicicletas y anclajes disponibles en cada una de ellas, actualizados cada dos minutos.

Junio 2018