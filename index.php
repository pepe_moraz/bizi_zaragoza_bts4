<?php 
session_start();

//llamamos al archivo de conexion a bbdd
require('includes/conexion.php');

//llamamos a las librerias que hay en /vendor/
require('vendor/autoload.php');

//llamamos al archivo que permite la gestión de las sesiones de usuarios
require('includes/login.php');

//Le decimos donde van a ir las plantillas de twig
$loader = new Twig_Loader_Filesystem('views/');


//ESTO PARA DESARROLLO
$twig = new Twig_Environment($loader);
$twig->addGlobal('session', $_SESSION);
//ESTO PARA PRODUCCION
//$twig = new Twig_Environment($loader, array('cache' => 'cache/'));
//$twig->addGlobal('session', $_SESSION);
//Vamos a pensar, a ver, a que controlador queremos llamar
//Por defecto, llamamos a blogController.php
if(isset($_GET['c'])){
  $c=$_GET['c'];
}else{
  $c='estacionesController.php';
}

//Llamo al controlador
require('controllers/'.$c);

//Desconectas
$conexion->close();
?>