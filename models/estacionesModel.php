<?php  
// Fichero models/estacionesModel.php
Class Estaciones{

	public $elementos;

	public function __construct(){
		$this->elementos=[];
	}

	public function dimeElementos(){
		$url='http://www.zaragoza.es/sede/servicio/urbanismo-infraestructuras/estacion-bicicleta.json';
		$datos=file_get_contents($url);
		$info=json_decode($datos);
		foreach ($info->result as $elemento) {
			$this->elementos[]=new Estacion($elemento);
		}
		return $this->elementos;
	}

	public function dimeElemento($id){
		$url='http://www.zaragoza.es/sede/servicio/urbanismo-infraestructuras/estacion-bicicleta.json';
		$datos=file_get_contents($url);
		$info=json_decode($datos);
		foreach ($info->result as $registro) {
			if(($registro->id)==$id){
				$elemento=new Estacion($registro);
			}
		}
		return $elemento;
	}

}