<?php  
// Fichero models/estacionModel.php
Class Estacion{
	public $id;
    public $nombre;
    public $bicis;
    public $anclajes;
    public $latitud;
	public $longitud;
	
	public function __construct($elemento){
		@$this->id=$elemento->id;
        @$this->nombre=$elemento->title;
        @$this->bicis=$elemento->bicisDisponibles;
        @$this->anclajes=$elemento->anclajesDisponibles;
        @$this->longitud=$elemento->geometry->coordinates[0];
        @$this->latitud=$elemento->geometry->coordinates[1];
	}
}

?>